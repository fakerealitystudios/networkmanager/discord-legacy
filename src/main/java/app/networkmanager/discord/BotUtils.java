package app.networkmanager.discord;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RequestBuffer;

import java.util.HashMap;
import java.util.Map;

public class BotUtils {

    // Constants for use throughout the bot
    public static String BOT_PREFIX = "%";
    public static final Map<Permissions, String> permsMapped;
    static {
        permsMapped = new HashMap<>();
        permsMapped.put(Permissions.MANAGE_CHANNELS, "Manage Channels");
        permsMapped.put(Permissions.MANAGE_MESSAGES, "Manage Messages");
        permsMapped.put(Permissions.MANAGE_EMOJIS, "Manage Emojis");
        permsMapped.put(Permissions.MANAGE_NICKNAMES, "Manage Nicknames");
        permsMapped.put(Permissions.MANAGE_ROLES, "Manage Roles");
        permsMapped.put(Permissions.MANAGE_SERVER, "Manage Server");
        permsMapped.put(Permissions.MANAGE_WEBHOOKS, "Manage Webhooks");
        permsMapped.put(Permissions.ADD_REACTIONS, "Add Reactions");
        permsMapped.put(Permissions.ADMINISTRATOR, "Administrator");
        permsMapped.put(Permissions.ATTACH_FILES, "Attach Files");
        permsMapped.put(Permissions.BAN, "Ban");
        permsMapped.put(Permissions.KICK, "Kick");
        permsMapped.put(Permissions.CHANGE_NICKNAME, "Change Nickname");
        permsMapped.put(Permissions.CREATE_INVITE, "Create Invite");
        permsMapped.put(Permissions.EMBED_LINKS, "Embed Links");
        permsMapped.put(Permissions.MENTION_EVERYONE, "Mention Everyone");
        permsMapped.put(Permissions.READ_MESSAGE_HISTORY, "Read Message History");
        permsMapped.put(Permissions.SEND_MESSAGES, "Send Messages");
        permsMapped.put(Permissions.SEND_TTS_MESSAGES, "Send TTS Messages");
        permsMapped.put(Permissions.USE_EXTERNAL_EMOJIS, "Use External Emojis");
        permsMapped.put(Permissions.VIEW_AUDIT_LOG, "View Audit Log");
        permsMapped.put(Permissions.VOICE_CONNECT, "Voice Connect");
        permsMapped.put(Permissions.VOICE_DEAFEN_MEMBERS, "Voice Deafen Members");
        permsMapped.put(Permissions.VOICE_MOVE_MEMBERS, "Voice Move Members");
        permsMapped.put(Permissions.VOICE_MUTE_MEMBERS, "Voice Mute Members");
        permsMapped.put(Permissions.VOICE_SPEAK, "Voice Speak");
        permsMapped.put(Permissions.VOICE_USE_VAD, "Voice Use VAD");

        permsMapped.put(Permissions.MANAGE_CHANNEL, "Manage Channel(Channel Specific)");
        permsMapped.put(Permissions.MANAGE_PERMISSIONS, "Manage Permissions(Channel Specific)");
        permsMapped.put(Permissions.READ_MESSAGES, "Read Messages(Channel Specific)");
    }

    // Handles the creation and getting of a IDiscordClient object for a token
    public static IDiscordClient getBuiltDiscordClient(String token){

        return new ClientBuilder()
                .withToken(token)
                .withRecommendedShardCount()
                .build();

    }

    public static void sendMessage(IChannel channel, String message){

        RequestBuffer.request(() -> {
            try{
                RequestBuffer.request(() -> channel.sendMessage(message));
            } catch (DiscordException e){
                System.err.println("Message could not be sent with error: ");
                e.printStackTrace();
            }
        });

    }

    public static void sendMessage(IChannel channel, EmbedObject message){

        if (channel == null) { return; }

        RequestBuffer.request(() -> {
            try{
                RequestBuffer.request(() -> channel.sendMessage(message));
            } catch (DiscordException e){
                System.err.println("Message could not be sent with error: ");
                e.printStackTrace();
            }
        });

    }

    public static void sendDebugMessage(String message) {
        //sendMessage(Main.DiscordClient.getChannelByID(491756231149748226L), message);
    }

}
