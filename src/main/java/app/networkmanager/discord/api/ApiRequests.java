package app.networkmanager.discord.api;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.api.responses.GenericResponse;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.*;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.Discord4J;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.util.LogMarkers;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

public class ApiRequests {

    /**
     * The user agent used for requests.
     */
    public static final String USER_AGENT = String.format("DiscordBot (NetworkManager, 1)");

    /**
     * Used to send POST requests.
     */
    public final Request POST;
    /**
     * Used to send GET requests.
     */
    public final Request GET;
    /**
     * Used to send DELETE requests.
     */
    public final Request DELETE;
    /**
     * Used to send PATCH requests.
     */
    public final Request PATCH;
    /**
     * Used to send PUT requests.
     */
    public final Request PUT;

    public ApiRequests() {
        POST = new Request(HttpPost.class);
        GET = new Request(HttpGet.class);
        DELETE = new Request(HttpDelete.class);
        PATCH = new Request(HttpPatch.class);
        PUT = new Request(HttpPut.class);
    }

    /**
     * A specific HTTP method request type.
     */
    public final class Request {

        /**
         * The HTTP client requests are made on.
         */
        //Same as HttpClients.createDefault() but with the proper user-agent
        private final CloseableHttpClient CLIENT = HttpClients.custom().setUserAgent(USER_AGENT).build();

        /**
         * The class of the method type used for the request
         */
        final Class<? extends HttpUriRequest> requestClass;

        private Request(Class<? extends HttpUriRequest> clazz) {
            this.requestClass = clazz;
        }

        /**
         * Makes a request.
         *
         * @param url The url to make the request to.
         * @param headers The headers to include in the request.
         * @return The response as a byte array.
         */
        public GenericResponse makeRequest(String url, IChannel channel, BasicNameValuePair... headers) {
            try {
                HttpUriRequest request = this.requestClass.getConstructor(String.class).newInstance(url);
                for (BasicNameValuePair header : headers) {
                    request.addHeader(header.getName(), header.getValue());
                }

                return request(request, channel);
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                Discord4J.LOGGER.error(LogMarkers.API, "Discord4J Internal Exception", e);
                return null;
            }
        }


        /**
         * Makes a request.
         *
         * @param url The url to make the request to.
         * @param entity Any data to serialize and send in the body of the request.
         * @param headers The headers to include in the request.
         * @return The response as a byte array.
         */
        public GenericResponse makeRequest(String url, IChannel channel, HttpEntity entity, BasicNameValuePair... headers) {
            try {
                if (HttpEntityEnclosingRequestBase.class.isAssignableFrom(this.requestClass)) {
                    HttpEntityEnclosingRequestBase request = (HttpEntityEnclosingRequestBase)
                            this.requestClass.getConstructor(String.class).newInstance(url);
                    for (BasicNameValuePair header : headers) {
                        request.addHeader(header.getName(), header.getValue());
                    }
                    request.setEntity(entity);
                    return request(request, channel);
                } else {
                    //LOGGER.error(LogMarkers.API, "Tried to attach HTTP entity to invalid type! ({})", this.requestClass.getSimpleName());
                    BotUtils.sendDebugMessage("Tried to attach HTTP entity to invalid type");
                }
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                Discord4J.LOGGER.error(LogMarkers.API, "Discord4J Internal Exception", e);
            }
            return null;
        }

        private GenericResponse request(HttpUriRequest request, IChannel channel) {
            try (CloseableHttpResponse response = CLIENT.execute(request)) {
                GenericResponse res = new GenericResponse(response);
                response.close();
                if (res.statusCode >= 500) {
                    BotUtils.sendMessage(channel, "Failed due to an internal webserver error! Check your current install.");
                    return null;
                }else if (res.statusCode >= 404) {
                    BotUtils.sendMessage(channel, "Failed to connect to Network Manager: Couldn't find api endpoint file. Make sure you have the correct file path.");
                    return null;
                }
                return res;
            } catch (IOException e) {
                BotUtils.sendMessage(channel, "Failed to connect to Network Manager: Couldn't establish a connection, perform a link or check your webserver.");
                return null;
            }
        }
    }

}
