package app.networkmanager.discord.api;

import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.configuration.GuildConfiguration;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import sx.blah.discord.handle.obj.*;

import java.io.UnsupportedEncodingException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Logs {
    private Logs() {}

    public enum LogType {
        SERVER,
        PLAYER;
    }

    public enum DetailType {
        NMID,
        USID,
        TOKENID,
        ENTITY,
        NPC, // Most of minecraft doesn't really have NPC's, we'll leave it here just in-case someone wants to extends our logs
        WEAPON,
        WORLD,

        NULL,
        TABLE,
        STRING,
        INTEGER,
        FLOAT;
    }

    public enum DamageType {
        EXPLOSION,
        SHOT,
        FALL,
        CRUSHED,
        SLASHED,
        BURNED,
        HIT,
        SHOCKED,
        DROWN,
        UNKNOWN,
        POISON,
        MAGIC,
        TRIGGER,
        VEHICLE;
    }

    private static class LogUser {
        IGuild guild;
        IUser user;
        int nmid;
        int usid;

        public LogUser(IGuild guild, IUser user) {
            this.guild = guild;
            this.user = user;
            this.nmid = Main.GetUserNetworkManagerId(guild, user);
            this.usid = -1;
            if (Main.UserSessions.get(guild.getStringID()).containsKey(user.getStringID())) {
                this.usid = Main.UserSessions.get(guild.getStringID()).get(user.getStringID()).sessionid;
            }
        }
    }

    private static class LogDetail {
        DetailType type;
        String key = null;
        Object value;

        public LogDetail(DetailType type, String key, Object value) {
            this.type = type;
            this.key = key;
            this.value = value;
        }

        public LogDetail(DetailType type, Object value) {
            this.type = type;
            this.value = value;
        }
    }

    /**
     * Will create a log event for NetworkManager
     *
     * @param type      The type of the log
     * @param event     The specific event, updated list available in NetworkManager documentation
     */
    public static void Log(IGuild guild, LogType type, String event) {
        Log(guild, type, event, null, null);
    }

    /**
     * Will create a log event for NetworkManager
     *
     * @param type      The type of the log
     * @param event     The specific event, updated list available in NetworkManager documentation
     * @param details   A of list of objects, which are then converted into the NetworkManager logDetails
     */
    public static void Log(IGuild guild, LogType type, String event, List<Object> details) {
        Log(guild, type, event, null, details);
    }

    /**
     * Will create a log event for NetworkManager
     *
     * @param type      The type of the log
     * @param event     The specific event, updated list available in NetworkManager documentation
     * @param subevent  The specific subevent, updated list available in NetworkManager documentation
     * @param details   A of list of objects, which are then converted into the NetworkManager logDetails
     */
    public static void Log(IGuild guild, LogType type, String event, String subevent, List<Object> details) {
        // Perform the details synchronously, in-case the objects may disappear next tick
        List<List<LogDetail>> new_details = new ArrayList<>();
        if (details != null) {
            for (Object obj : details) {
                if (obj instanceof IUser) {
                    obj = new LogUser(guild, (IUser)obj);
                }
                new_details.add(FormatDetail(obj));
            }
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                PerformLog(guild, type, event, subevent, new_details);
            }
        }).start();
    }

    public synchronized static void LogSynchronously(IGuild guild, LogType type, String event) {
        LogSynchronously(guild, type, event, null, null);
    }

    public synchronized static void LogSynchronously(IGuild guild, LogType type, String event, List<Object> details) {
        LogSynchronously(guild, type, event, null, details);
    }

    public synchronized static void LogSynchronously(IGuild guild, LogType type, String event, String subevent, List<Object> details) {
        List<List<LogDetail>> new_details = new ArrayList<>();
        if (details != null) {
            for (Object obj : details) {
                if (obj instanceof IUser) {
                    obj = new LogUser(guild, (IUser)obj);
                }
                new_details.add(FormatDetail(obj));
            }
        }

        PerformLog(guild, type, event, subevent, new_details);
    }

    private static void PerformLog(IGuild guild, LogType type, String event, String subevent, List<List<LogDetail>> details) {
        final GuildConfiguration config = Main.GetGuildConfiguration(guild.getStringID());

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("type", type.toString()));
        params.add(new BasicNameValuePair("event", event));
        params.add(new BasicNameValuePair("subevent", subevent));
        params.add(new BasicNameValuePair("time", Integer.toString((int) (System.currentTimeMillis() / 1000L))));
        org.json.JSONArray array = new org.json.JSONArray();
        for (int i = 0; i < details.size(); i++) {
            for (LogDetail detail : details.get(i)) {
                JSONObject obj = new JSONObject();
                obj.put("type", detail.type.toString());
                obj.put("argid", i);
                if (detail.key != null) {
                    obj.put("vid", detail.key);
                }
                obj.put("value", detail.value.toString());
                array.put(obj);
            }
        }
        params.add(new BasicNameValuePair("logDetails", array.toString()));
        UrlEncodedFormEntity ent = null;
        try {
            ent = new UrlEncodedFormEntity(params);
        } catch(UnsupportedEncodingException e) {
            return;
        }

        GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=log", null, ent, new BasicNameValuePair("Authorization", config.secret));
        System.out.println("Received content: " + response.content);
        System.out.println("Received status: " + response.statusCode);
    }

    private static List<LogDetail> FormatDetail(Object obj) {
        List<LogDetail> result = new ArrayList<>();
        if (obj instanceof LogUser) {
            final LogUser user = (LogUser) obj;
            result.add(new LogDetail(DetailType.NMID, user.nmid));
            if (user.usid != -1) {
                result.add(new LogDetail(DetailType.USID, user.usid));
            }
            result.add(new LogDetail(DetailType.STRING, "name", user.user.getDisplayName(user.guild)));
        } else if (obj instanceof IGuild) {
            final IGuild guild = (IGuild) obj;
            result.add(new LogDetail(DetailType.TOKENID, "discord_guildid", guild.getStringID()));
            result.add(new LogDetail(DetailType.STRING, "name", guild.getName()));
        } else if (obj instanceof IChannel) {
            final IChannel channel = (IChannel) obj;
            result.add(new LogDetail(DetailType.TOKENID, "discord_channelid", channel.getStringID()));
            String channelType = "text";
            if (obj instanceof IVoiceChannel) {
                channelType = "voice";
            }
            result.add(new LogDetail(DetailType.STRING, "type", channelType));
            result.add(new LogDetail(DetailType.STRING, "name", channel.getName()));
        } else if (obj instanceof IMessage) {
            final IMessage msg = (IMessage) obj;
            result.add(new LogDetail(DetailType.TOKENID, "discord_messageid", msg.getStringID()));
        } else if (obj instanceof String) {
            result.add(new LogDetail(DetailType.STRING, (String)obj));
        } else if (obj instanceof Integer) {
            result.add(new LogDetail(DetailType.INTEGER, (Integer)obj));
        } else if (obj instanceof Long) {
            result.add(new LogDetail(DetailType.INTEGER, (Long)obj));
        } else if (obj instanceof Float) {
            result.add(new LogDetail(DetailType.FLOAT, (Float)obj));
        } else if (obj instanceof Double) {
            result.add(new LogDetail(DetailType.FLOAT, (Double)obj));
        } else if (obj instanceof Map) {
            // Convert into JSON
            JSONObject json = new JSONObject();
            json.putAll((Map)obj);
            result.add(new LogDetail(DetailType.STRING, json.toJSONString()));
        } else if (obj instanceof List) {
            // Convert into JSON
            JSONArray json = new JSONArray();
            for (int i = 0; i < ((List)obj).size(); i++) {
                json.add(((List)obj).get(i));
            }
            result.add(new LogDetail(DetailType.STRING, json.toJSONString()));
        }
        return result;
    }
}
