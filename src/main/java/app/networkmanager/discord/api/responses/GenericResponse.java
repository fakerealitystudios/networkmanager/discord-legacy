package app.networkmanager.discord.api.responses;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class GenericResponse {
    public int statusCode;
    public String content;

    public GenericResponse(CloseableHttpResponse response) {
        statusCode = response.getStatusLine().getStatusCode();
        try {
            content = EntityUtils.toString(response.getEntity());
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
}
