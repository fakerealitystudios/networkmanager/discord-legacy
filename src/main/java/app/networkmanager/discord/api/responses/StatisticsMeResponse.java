package app.networkmanager.discord.api.responses;

public class StatisticsMeResponse {
    public String id;
    public String url;
    public String displayName;
    public String avatar;
    public String playtime = "None";
    public String sessions = "0";
    public String kills = "0";
    public String deaths = "0";
}
