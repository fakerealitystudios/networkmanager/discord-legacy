package app.networkmanager.discord.api.responses;

public class RankResponse {
    public String rank;
    public String displayName;
    public String inherit;
    public String realm;
    public String[] permissions;
    public Integer subgrouping;
}
