package app.networkmanager.discord.api.responses;

public class RefactorResponse {
    public User[] users;

    public static class User {
        public String memberToken;
        public MemberResponse.Rank[] ranks;
        public String[] badges;
    }
}
