package app.networkmanager.discord.api.responses;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

public class MemberResponse {
	public String id;
	@JsonIgnoreProperties
	public String url;
	@JsonIgnoreProperties
	public String displayName;
	@JsonIgnoreProperties
	public String avatar;
	@JsonIgnoreProperties
	public Rank[] ranks;
	@JsonIgnoreProperties
    public String logs;
	@JsonIgnoreProperties
	public String[] badges;

    public static class Rank {
        public String realm = "";
        public String rank = "";
    }
}
