package app.networkmanager.discord.api.responses;

public class StatisticsResponse {
    public StatisticsValue[] top;
    public StatisticsValue me;
}
