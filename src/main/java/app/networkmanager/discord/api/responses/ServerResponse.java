package app.networkmanager.discord.api.responses;

public class ServerResponse {
    public String name;
    public String game;
    public String address;

    public String getAddress() {
        String text = "";
        if (game.equalsIgnoreCase("garrysmod")) {
            text += "steam://connect/" + address;
        } else {
            text += address;
        }
        return text;
    }

    public String toString() {
        String text = name + " - " + game;
        text += "\n\t- ";
        text += getAddress();
        return text;
    }
}
