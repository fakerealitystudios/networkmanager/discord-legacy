package app.networkmanager.discord.listener;

import app.networkmanager.discord.Main;
import app.networkmanager.discord.configuration.GuildConfiguration;
import org.json.JSONObject;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IUser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Iterator;
import java.util.Map;

public class SocketListener extends Thread {
    int portNumber;
    public ServerSocket serverSocket;
    public SocketListener(int portNumber) {
        this.portNumber = portNumber;
    }

    public void run() {
        System.out.println("Starting socket listener");
        try {
            serverSocket = new ServerSocket(portNumber);
            while (!Thread.currentThread().isInterrupted()) {
                new SocketProcessor(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Ending socket listener");
    }
}


class SocketProcessor extends Thread {
    private Socket socket;

    SocketProcessor(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        System.out.println("Received socket client");
        try {
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String input = in.readLine();
            if (input == null) {
                return;
            }
            JSONObject json = new JSONObject(input);

            String panel = json.getString("panel");
            // Need to get the guild id from this panel...
            System.out.println(panel);
            for (Map.Entry<String, GuildConfiguration> entry : Main.GuildConfigurations.entrySet()) {
                if (entry.getValue().endpoint.equalsIgnoreCase(panel)) {
                    IGuild guild = Main.DiscordClient.getGuildByID(Long.parseLong(entry.getKey()));
                    if (guild == null) {
                        System.out.println("Failed to find GuildId from panel endpoint!");
                    } else {
                        System.out.println("Found guild! " + guild.getName());

                        String action = json.getString("action");
                        System.out.println("Processing new payload, action: " + action);

                        if (action.equals("kick")) {

                        } else if (action.equals("ban")) {

                        } else if (action.equals("setRank")) {
                            Main.CollectUser(guild, Main.GetUserDiscordUser(guild, json.getInt("nmid")));
                        } else if (action.equals("removeRank")) {
                            Main.CollectUser(guild, Main.GetUserDiscordUser(guild, json.getInt("nmid")));
                        } else if (action.equals("merge")) {
                            Main.CollectUser(guild, Main.GetUserDiscordUser(guild, json.getInt("merged_nmid")));
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}