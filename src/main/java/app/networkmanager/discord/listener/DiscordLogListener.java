package app.networkmanager.discord.listener;

import app.networkmanager.discord.Main;
import app.networkmanager.discord.UserSession;
import app.networkmanager.discord.api.Logs;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class DiscordLogListener {

    @EventSubscriber
    public void onUserVoiceChannelJoinEvent(UserVoiceChannelJoinEvent event) {
        if (event.getVoiceChannel() == event.getGuild().getAFKChannel()) {
            // We don't treat the afk channel as something we care about
            return;
        }
        Map<String, UserSession> guildSessions = Main.UserSessions.get(event.getGuild().getStringID());
        if (guildSessions.containsKey(event.getUser().getStringID())) {
            guildSessions.get(event.getUser().getStringID()).close();
        }
        guildSessions.put(event.getUser().getStringID(), new UserSession(event.getGuild(), Main.GetUserNetworkManagerId(event.getGuild(), event.getUser())));
        List<Object> details = new ArrayList<>();
        details.add(event.getUser());
        details.add(event.getVoiceChannel());
        Logs.Log(event.getGuild(), Logs.LogType.PLAYER, "connect", details);
    }

    @EventSubscriber
    public void onUserVoiceChannelLeaveEvent(UserVoiceChannelLeaveEvent event) {
        if (event.getVoiceChannel() == event.getGuild().getAFKChannel()) {
            // We don't treat the afk channel as something we care about
            return;
        }
        List<Object> details = new ArrayList<>();
        details.add(event.getUser());
        details.add(event.getVoiceChannel());
        Logs.Log(event.getGuild(), Logs.LogType.PLAYER, "disconnect", details);

        Map<String, UserSession> guildSessions = Main.UserSessions.get(event.getGuild().getStringID());
        if (guildSessions.containsKey(event.getUser().getStringID())) {
            guildSessions.get(event.getUser().getStringID()).close();
        }
    }

    @EventSubscriber
    public void onUserVoiceChannelMoveEvent(UserVoiceChannelMoveEvent event) {
        if (event.getOldChannel() != event.getGuild().getAFKChannel()) {
            List<Object> details = new ArrayList<>();
            details.add(event.getUser());
            details.add(event.getOldChannel());
            Logs.Log(event.getGuild(), Logs.LogType.PLAYER, "disconnect", details);

            Map<String, UserSession> guildSessions = Main.UserSessions.get(event.getGuild().getStringID());
            if (guildSessions.containsKey(event.getUser().getStringID())) {
                guildSessions.get(event.getUser().getStringID()).close();
            }
        }

        if (event.getVoiceChannel() != event.getGuild().getAFKChannel()) {
            Map<String, UserSession> guildSessions = Main.UserSessions.get(event.getGuild().getStringID());
            guildSessions.put(event.getUser().getStringID(), new UserSession(event.getGuild(), Main.GetUserNetworkManagerId(event.getGuild(), event.getUser())));
            List<Object> details = new ArrayList<>();
            details.add(event.getUser());
            details.add(event.getVoiceChannel());
            Logs.Log(event.getGuild(), Logs.LogType.PLAYER, "connect", details);
        }
    }

    @EventSubscriber
    public void onMessageReceivedEvent(MessageReceivedEvent event) {
        if (event.getMessage().isDeleted()) {
            /*List<Object> details = new ArrayList<>();
            details.add(event.getAuthor());
            details.add(event.getMessage());
            details.add(event.getChannel());
            Logs.Log(event.getGuild(), Logs.LogType.PLAYER, "chat", "deleted", details);*/
        } else if (!event.getMessage().getFormattedContent().isEmpty()) {
            List<Object> details = new ArrayList<>();
            details.add(event.getAuthor());
            details.add(event.getMessage().getFormattedContent());
            details.add(event.getChannel());
            Logs.Log(event.getGuild(), Logs.LogType.PLAYER, "chat", details);
        }
    }
}
