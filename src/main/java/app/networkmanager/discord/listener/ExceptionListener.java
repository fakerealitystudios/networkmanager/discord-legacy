package app.networkmanager.discord.listener;

import app.networkmanager.discord.BotUtils;

public class ExceptionListener implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        BotUtils.sendDebugMessage("Unhandled Exception: " + e.getMessage());
    }
}
