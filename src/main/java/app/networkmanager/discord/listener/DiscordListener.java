package app.networkmanager.discord.listener;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.api.responses.MemberResponse;
import app.networkmanager.discord.api.responses.ErrorResponse;
import app.networkmanager.discord.commands.*;
import app.networkmanager.discord.configuration.ChannelConfiguration;
import app.networkmanager.discord.configuration.GuildConfiguration;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import org.json.JSONArray;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.GuildUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageSendEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.impl.events.shard.ReconnectSuccessEvent;
import sx.blah.discord.handle.impl.events.shard.ShardReadyEvent;
import sx.blah.discord.handle.obj.*;
import sx.blah.discord.util.MissingPermissionsException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import java.sql.PreparedStatement;
import java.time.Instant;
import java.util.*;

public class DiscordListener {
	public static Map<String, BaseCommand> commandMap = new TreeMap<>();
	public static Instant ping;

	static {
		addCommand(commandMap, new HelpCommand());
		addCommand(commandMap, new GuildMultiCommand());
		addCommand(commandMap, new ChannelMultiCommand());
		addCommand(commandMap, new ServersCommand());
		addCommand(commandMap, new InfoSubCommand());
		addCommand(commandMap, new PanelCommand());
		addCommand(commandMap, new LinkCommand());
		addCommand(commandMap, new DebugSubCommand());
		addCommand(commandMap, new PermissionsCommand());
		addCommand(commandMap, new LogsMultiCommand());
		addCommand(commandMap, new PingCommand());
		addCommand(commandMap, new StatsSubCommand());
		addCommand(commandMap, new WhoisCommand());
		addCommand(commandMap, new AboutCommand());
	}

	public static void addCommand(Map<String, BaseCommand> map, BaseCommand cmd) {
		map.put(cmd.getCommand(), cmd);
	}

	@EventSubscriber
	public void onReadyEvent(ReadyEvent event) {
		BotUtils.sendDebugMessage("Bot has been fully initialized with " + event.getClient().getGuilds().size()
				+ " guilds via " + event.getClient().getShards().size() + " shards");
	}

	@EventSubscriber
	public void onShardReadyEvent(ShardReadyEvent event) {
		event.getClient().changePresence(StatusType.ONLINE, ActivityType.PLAYING, "Network Management");
		BotUtils.sendDebugMessage("Shard " + event.getShard().getInfo()[0] + ": Bot has logged in to "
				+ event.getClient().getGuilds().size() + " guilds");
	}

	@EventSubscriber
	public void onShardReconnectSuccessEvent(ReconnectSuccessEvent event) {
		BotUtils.sendDebugMessage("Shard " + event.getShard().getInfo()[0] + ": Reconnected");
	}

	@EventSubscriber
	public void onGuildCreateEvent(GuildCreateEvent event) {
		// Called every time the bot is turned on per-guild or when added to a guild
		Main.LoadGuildConfiguration(event.getGuild().getStringID());
		GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
		if (!config.name.equalsIgnoreCase(event.getGuild().getName())) {
			config.name = event.getGuild().getName();
			config.writeYaml();
		}
		Main.UserSessions.put(event.getGuild().getStringID(), new HashMap<>());

		// Get server badges
		GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=badges", null, new BasicNameValuePair("Authorization", config.secret));
        System.out.println("Received content: " + response.content);
        System.out.println("Received status: " + response.statusCode);
        if (response.statusCode == 200) {
            JSONArray obj = new JSONArray(response.content);
            for (int i=0;i<obj.length();i++) {
                if (!config.badgesMap.containsKey(obj.getString(i))) {
                    IRole role = event.getGuild().createRole();
                    role.changeName(obj.getString(i));
                    config.badgesMap.put(obj.getString(i), role.getStringID());
                }
            }
            config.writeYaml();
        }
	}

	@EventSubscriber
	public void onGuildUpdateEvent(GuildUpdateEvent event) {
		GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
		if (!config.name.equalsIgnoreCase(event.getNewGuild().getName())) {
			config.name = event.getNewGuild().getName();
			config.writeYaml();
		}
	}

	@EventSubscriber
	public void onGuildLeaveEvent(GuildLeaveEvent event) {
		// Called every time the bot leaves a guild(most likely by force)
		BotUtils.sendDebugMessage("Bot has been removed from the guild " + event.getGuild().getName() + "!");
	}

	// When a user joins, grab their NMID
	@EventSubscriber
	public void onUserJoinEvent(UserJoinEvent event) {
		Main.CollectUser(event.getGuild(), event.getUser());
	}

	@EventSubscriber
	public void onMessageSendEvent(MessageSendEvent event) {
		if (event.getMessage().getContent().equalsIgnoreCase("pong")) {
			event.getMessage().edit(
					"Bot's current ping(round-trip) is " + (Instant.now().toEpochMilli() - ping.toEpochMilli()) + "ms");
			ping = null;
		}
	}

	@EventSubscriber
	public void onMessageReceivedEvent(MessageReceivedEvent event) {

		System.out.println("Received new message " + event.getMessage().getContent());

		// Given a message "/test arg1 arg2", argArray will contain ["/test", "arg1",
		// "arg"]
		String[] argArray = event.getMessage().getContent().split(" ");

		// First ensure at least the command and prefix is present, the arg length can
		// be handled by your command func
		if (argArray.length == 0)
			return;

		// Check if the first arg (the command) starts with the prefix defined in the
		// utils class
		if (!argArray[0].startsWith(BotUtils.BOT_PREFIX))
			return;

		// Extract the "command" part of the first arg out by ditching the amount of
		// characters present in the prefix
		String commandStr = argArray[0].substring(BotUtils.BOT_PREFIX.length()).toLowerCase();

		final ChannelConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID())
				.getChannel(event.getChannel().getStringID());

		// Reject chats in ignored channels.
		if (config.ignored && !commandStr.equalsIgnoreCase("channel")) { // Don't use commands here unless the channel
																			// command is run
			System.out.println("Rejecting messaging in ignored channel '" + event.getChannel().getName() + "'");
			return;
		}

		// Load the rest of the args in the array into a List for safer access
		List<String> argsList = new ArrayList<>(Arrays.asList(argArray));
		argsList.remove(0);

		if (commandMap.containsKey(commandStr)) {
			BaseCommand cmd = commandMap.get(commandStr);
			DiscordListener.attemptCommand(event, argsList, cmd);
		} else {
			// Log commands that don't exist
			BotUtils.sendDebugMessage("Not-Found: DiscordListener attempted '" + commandStr + "'");

			ExtractedResult approximate = FuzzySearch.extractOne(commandStr, commandMap.keySet());
			BotUtils.sendMessage(event.getChannel(),
					"The command '" + commandStr + "' does not exist! Did you mean '" + approximate.getString() + "'?");
		}
	}

	public static void attemptCommand(MessageReceivedEvent event, List<String> args, BaseCommand cmd) {
		EnumSet<Permissions> perms = event.getAuthor().getPermissionsForGuild(event.getGuild());
		if (perms.containsAll(cmd.requiredPermissions())) {
			try {
				cmd.run(event, args);
			} catch (MissingPermissionsException e) {
				BotUtils.sendDebugMessage(
						"MissingPermission: Attempted to run '" + cmd.getCommand() + "' | " + e.getErrorMessage());
			}
			if (cmd.shouldDeleteMessage() && !event.getChannel().isPrivate()) {
				event.getMessage().delete();
			}
		} else {
			String[] words = { "bigger", "better", "cooler", "immense", "vast", "huge", "hefty", "ample", "perfect",
					"admin", "staff" };
			Random generator = new Random();
			int randomIndex = generator.nextInt(words.length);
			BotUtils.sendMessage(event.getChannel(),
					"Sorry, but you require " + words[randomIndex] + " permissions for that command.");
		}
	}
}
