package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class InfoSubCommand extends BaseSubCommand {
    public String getCommand() {
        return "info";
    }

    public Map<String, String> getSubCommands()
    {
        Map<String, String> map = new TreeMap<>();
        map.put("guildid", "Returns the current guild id");
        map.put("channelid", "Returns the current channel id");
        map.put("userid", "Returns the current user id");
        return map;
    }

    public void runSub(MessageReceivedEvent event, String cmd, List<String> args) {
        switch (cmd) {
            case "guildid":
                if (event.getChannel().isPrivate()) {
                    BotUtils.sendMessage(event.getChannel(), "Private channels don't have a guild id!");
                } else {
                    BotUtils.sendMessage(event.getChannel(), "The current guild's id is " + event.getGuild().getStringID());
                }
                break;
            case "channelid":
                BotUtils.sendMessage(event.getChannel(), "The current channel's id is " + event.getChannel().getStringID());
                break;
            case "userid":
                BotUtils.sendMessage(event.getChannel(), "Your id is " + event.getAuthor().getStringID());
                break;
        }
    }
}
