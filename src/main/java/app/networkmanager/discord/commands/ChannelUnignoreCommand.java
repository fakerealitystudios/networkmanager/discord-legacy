package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.configuration.ChannelConfiguration;
import app.networkmanager.discord.configuration.GuildConfiguration;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class ChannelUnignoreCommand extends BaseCommand {
    public String getCommand() {
        return "unignore";
    }

    public String getHelp() { return "Listen to all commands sent in this channel"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
        final ChannelConfiguration channel = config.getChannel(event.getChannel().getStringID());
        channel.ignored = false;
        config.writeYaml(); // Save changes
        BotUtils.sendMessage(event.getChannel(), "Sure thing, I will listen to commands from #" + event.getChannel().getName() + " again");
    }
}
