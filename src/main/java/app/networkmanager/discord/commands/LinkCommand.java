package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.configuration.GuildConfiguration;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

import java.util.List;

public class LinkCommand extends BaseCommand {
    public String getCommand() {
        return "link";
    }

    public String getHelp() { return "Provides link instructions"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
        if (config.secret.isEmpty()) {
            BotUtils.sendMessage(event.getChannel(), "Can't perform link, a NetworkManager panel hasn't been linked to this guild yet! An Administrator should run %guild link");
            return;
        }

        EmbedBuilder builder = new EmbedBuilder();
        String content = "Open <" + config.endpoint + "> and login." +
                "\n\tHaven't created an account? Don't worry! If you've played a server you already have one!" +
                "\n\tYou simply need to use a button on the right to login to your existing account, such as Steam or Minecraft." +
                "\nAfter logging in, simply go to <" + config.endpoint + "profile.php> and go to *Connected Accounts*" +
                "\nFind Discord and press Link and follow directions on Discord.\n";
        builder.appendField("To link your Discord Id to your NetworkManager Id -", content, false);
        builder.withColor(36,192,235);
        BotUtils.sendMessage(event.getChannel(), builder.build());
    }
}