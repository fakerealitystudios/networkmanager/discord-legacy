package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.util.EmbedBuilder;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DebugSubCommand extends BaseSubCommand {
	public boolean isSecret() {
		return true;
	}

	public String getCommand() {
		return "debug";
	}

	public Map<String, String> getSubCommands() {
		Map<String, String> map = new TreeMap<>();
		map.put("roles", "Lists roles of the current guild");
		map.put("channels", "Lists channels of the current guild");
		map.put("test", "Whatever I currently set to be run in there");
		map.put("users", "Users");
		return map;
	}

	public void runSub(MessageReceivedEvent event, String cmd, List<String> args) {
		if (!event.getAuthor().getStringID().equalsIgnoreCase("164909237011611648")) {
			return;
		} // Debug command is only for Madkiller Max

		String text;
		switch (cmd) {
		case "roles":
			text = "```";
			for (IRole role : event.getGuild().getRoles()) {
				text += role.getName() + " - " + role.getStringID();
				if (role.isManaged()) {
					text += " - Managed";
				}
				text += "\n";
			}
			text += "```";
			BotUtils.sendMessage(event.getChannel(), text);
			break;
		case "channels":
			text = "```";
			for (IChannel channel : event.getGuild().getChannels()) {
				text += channel.getName() + " - " + channel.getStringID();
				text += "\n";
			}
			text += "```";
			BotUtils.sendMessage(event.getChannel(), text);
			break;
		case "test":
			EmbedBuilder builder = new EmbedBuilder();

			builder.appendField("fieldTitleInline", "fieldContentInline", true);
			builder.appendField("fieldTitleInline2", "fieldContentInline2", true);
			builder.appendField("fieldTitleNotInline", "fieldContentNotInline", false);
			builder.appendField(":tada: fieldWithCoolThings :tada:", "[hiddenLink](http://i.imgur.com/Y9utuDe.png)",
					false);

			builder.withAuthorName("authorName");
			builder.withAuthorIcon("http://i.imgur.com/PB0Soqj.png");
			builder.withAuthorUrl("http://i.imgur.com/oPvYFj3.png");

			builder.withColor(255, 0, 0);
			builder.withDesc("withDesc");
			builder.withDescription("withDescription");
			builder.withTitle("withTitle");
			builder.withTimestamp(100);
			builder.withUrl("http://i.imgur.com/IrEVKQq.png");
			builder.withImage("http://i.imgur.com/agsp5Re.png");

			builder.withFooterIcon("http://i.imgur.com/Ch0wy1e.png");
			builder.withFooterText("footerText");
			builder.withFooterIcon("http://i.imgur.com/TELh8OT.png");
			builder.withThumbnail("http://i.imgur.com/7heQOCt.png");

			builder.appendDesc(" + appendDesc");
			builder.appendDescription(" + appendDescription");

			BotUtils.sendMessage(event.getChannel(), builder.build());
			break;
		}
	}
}
