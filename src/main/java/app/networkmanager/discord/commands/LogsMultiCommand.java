package app.networkmanager.discord.commands;

import app.networkmanager.discord.listener.DiscordListener;
import sx.blah.discord.handle.obj.Permissions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class LogsMultiCommand extends BaseMultiCommand {
    public String getCommand() {
        return "logs";
    }

    public List<Permissions> requiredPermissions() {
        return Arrays.asList(Permissions.MANAGE_SERVER);
    }

    public String getHelp() { return ""; }
}
