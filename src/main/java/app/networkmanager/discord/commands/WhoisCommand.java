package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.ErrorResponse;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.api.responses.MemberResponse;
import app.networkmanager.discord.configuration.GuildConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.EmbedBuilder;

import java.util.ArrayList;
import java.util.List;

public class WhoisCommand extends BaseCommand {
    public String getCommand() {
        return "whois";
    }

    public String getHelp() { return "Tells ya about someone with NetworkManager data"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
        if (config.secret.isEmpty()) {
            BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: Your panel hasn't been linked yet!");
            return;
        }

        List<IUser> users = event.getClient().getUsersByName(String.join(" ", args), true);
        if (users.size() == 0) {
            BotUtils.sendMessage(event.getChannel(), "Found no Discord user with that name!");
            return;
        }

		List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("memberLink", "discord"));
        params.add(new BasicNameValuePair("memberToken", users.get(0).getStringID()));
        event.getChannel().setTypingStatus(true);
        try {
            GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=whois", event.getChannel(), new UrlEncodedFormEntity(params), new BasicNameValuePair("Authorization", config.secret));
            System.out.println("Received content: " + response.content);
            System.out.println("Received status: " + response.statusCode);
            if (response.statusCode == 200) {
                ObjectMapper mapper = new ObjectMapper();
                MemberResponse mappedResponse = mapper.readValue(response.content, MemberResponse.class);

                EmbedBuilder builder = new EmbedBuilder();
                builder.withAuthorUrl(mappedResponse.url);
                builder.withAuthorIcon(mappedResponse.avatar);
                builder.withAuthorName(mappedResponse.displayName);

                builder.appendField("NetworkManager Id", mappedResponse.id, false);
                builder.appendField("Involved logs", mappedResponse.logs, false);
                String info = "";
                if (mappedResponse.ranks.length == 0) {
                    info = "user\n";
                } else {
                    for (MemberResponse.Rank rank : mappedResponse.ranks) {
                        info += rank.rank + "\n";
                    }
                }
                builder.appendField("Ranks", info, true);
                info = "";
                if (mappedResponse.ranks.length == 0) {
                    info = "*";
                } else {
                    for (MemberResponse.Rank rank : mappedResponse.ranks) {
                        info += rank.realm + "\n";
                    }
                }
                builder.appendField("Realms", info, true);

                builder.withColor(36,192,235);
                BotUtils.sendMessage(event.getChannel(), builder.build());
            } else {
                ObjectMapper mapper = new ObjectMapper();
                ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: " + errorResponse.error);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}