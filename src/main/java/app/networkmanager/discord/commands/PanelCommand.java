package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;

public class PanelCommand extends BaseCommand {
    public String getCommand() {
        return "panel";
    }

    public String getHelp() { return "Provides the website to the linked network manager panel"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        BotUtils.sendMessage(event.getChannel(), "Network Manager is located at: " + Main.GetGuildConfiguration(event.getGuild().getStringID()).endpoint);
    }
}