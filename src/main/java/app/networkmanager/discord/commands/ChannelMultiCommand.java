package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.listener.DiscordListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;

import java.util.*;

public class ChannelMultiCommand extends BaseMultiCommand {
    public String getCommand() {
        return "channel";
    }

    {
        DiscordListener.addCommand(subCommands, new ChannelIgnoreCommand());
        DiscordListener.addCommand(subCommands, new ChannelUnignoreCommand());
        DiscordListener.addCommand(subCommands, new ChannelClearCommand());
    }

    public List<Permissions> requiredPermissions() {
        return Arrays.asList(Permissions.MANAGE_CHANNEL);
    }

    public String getHelp() { return ""; }
}
