package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.listener.DiscordListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.time.Instant;
import java.util.Date;
import java.util.List;

public class PingCommand extends BaseCommand {
    public String getCommand() {
        return "ping";
    }

    public String getHelp() { return "Displays the bot's ping to this guild"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        DiscordListener.ping = Instant.now();
        BotUtils.sendMessage(event.getChannel(), "pong");
    }
}
