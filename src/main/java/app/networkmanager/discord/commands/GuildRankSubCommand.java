package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.*;
import app.networkmanager.discord.configuration.GuildConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.Permissions;
import sx.blah.discord.util.RequestBuffer;

import java.io.IOException;
import java.util.*;

public class GuildRankSubCommand extends BaseSubCommand {
    public String getCommand() {
        return "ranks";
    }

    public Map<String, String> getSubCommands()
    {
        Map<String, String> map = new TreeMap<>();
        map.put("refactor", "Reset user ranks");
        map.put("map", "Map a rank to a discord role");
        map.put("list", "List NetworkManager ranks");
        return map;
    }

    public String getHelp() { return ""; }

    public List<Permissions> requiredPermissions() {
        return Arrays.asList(Permissions.MANAGE_ROLES);
    }

    public void runSub(MessageReceivedEvent event, String cmd, List<String> args) {
        final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());

        switch (cmd) {
            case "refactor":
                // TODO: Perform refactoring in batches
                if (config.ranksMap.size() <= 0) {
                    BotUtils.sendMessage(event.getChannel(), "No ranks have been mapped yet! Use the guild rank map command to perform rank mapping.");
                    return;
                }

                BotUtils.sendMessage(event.getChannel(),"Requesting data from panel, this may take a while. How about you get some coffee?");
                for (IUser user : event.getGuild().getUsers()) {
                    Main.CollectUser(event.getGuild(), user);
                }
                BotUtils.sendMessage(event.getChannel(),"Received all user data and completing rank refactoring. May reach Discord rate limits, so some changes may queue up.");
                break;
            case "map": {
                if (args.size() != 2) {
                    BotUtils.sendMessage(event.getChannel(), "To map a rank to a discord role, provide the NetworkManager rank, then mention the Discord role.");
                    return;
                }
                if (event.getMessage().getRoleMentions().size() != 1) {
                    BotUtils.sendMessage(event.getChannel(), "You must use an @Role to mention the role you want to map!");
                    return;
                }

                GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=ranks", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
                System.out.println("Received content: " + response.content);
                System.out.println("Received status: " + response.statusCode);
                if (response.statusCode == 200) {
                    try {
                        ObjectMapper mapper = new ObjectMapper();
                        RanksResponse mappedResponse = mapper.readValue(response.content, RanksResponse.class);
                        boolean bMapped = false;
                        for (RankResponse rank : mappedResponse.ranks) {
                            if (args.get(0).equalsIgnoreCase(rank.rank)) {
                                config.ranksMap.put(rank.rank, event.getMessage().getRoleMentions().get(0).getStringID());
                                config.rolesMap.put(event.getMessage().getRoleMentions().get(0).getStringID(), rank.rank);
                                config.writeYaml();
                                bMapped = true;
                                break;
                            } else if (args.get(1).equalsIgnoreCase(rank.rank)) {
                                config.ranksMap.put(rank.rank, event.getMessage().getRoleMentions().get(0).getStringID());
                                config.rolesMap.put(event.getMessage().getRoleMentions().get(0).getStringID(), rank.rank);
                                config.writeYaml();
                                bMapped = true;
                                break;
                            }
                        }
                        if (bMapped) {
                            BotUtils.sendMessage(event.getChannel(), "Mapped NetworkManager rank to Discord role!");
                        } else {
                            BotUtils.sendMessage(event.getChannel(), "Failed to map NetworkManager rank to Discord role!");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {

                }
                break;
            }
            case "list": {
                GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=ranks", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
                try {
                    System.out.println("Received content: " + response.content);
                    System.out.println("Received status: " + response.statusCode);
                    if (response.statusCode == 200) {
                        ObjectMapper mapper = new ObjectMapper();
                        RanksResponse mappedResponse = mapper.readValue(response.content, RanksResponse.class);
                        String text = "";
                        for (RankResponse rank : mappedResponse.ranks) {
                            text += rank.displayName + "\n";
                        }
                        BotUtils.sendMessage(event.getChannel(), text);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }
}