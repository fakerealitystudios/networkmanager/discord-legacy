package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.ErrorResponse;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.api.responses.TokenResponse;
import app.networkmanager.discord.configuration.GuildConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.io.IOException;
import java.util.List;

public class GuildLinkCommand extends BaseCommand {
    public String getCommand() {
        return "link";
    }

    public String getHelp() { return "Links the bot for this guild to your panel"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        if (args.size() != 2) {
            BotUtils.sendMessage(event.getChannel(), "To link the bot to your Network Manager panel - " +
                    "```Open your Network Manager web panel\n" +
                    "Go to Settings -> General\n" +
                    "Now click Generate Discord Bot Token\n" +
                    "Now run this command providing the token and then the web panel address" +
                    "\n\ni.e.\n!link XXXXXXXXXX https://demo.networkmanager.io/```" +
                    "This command is safe in a public discord, the token will be regenerated and stored upon a successful connection.");
        } else {
            event.getMessage().delete();
            event.getChannel().setTypingStatus(true);
            String token = args.get(0);
            String endpoint = args.get(1);
            if (!endpoint.substring(endpoint.length()-1).equals("/"))
                endpoint += "/";

            GenericResponse response = Main.Api.POST.makeRequest(endpoint + "api.php?realm=discord&action=link", event.getChannel(), new BasicNameValuePair("Authorization", token));
            if (response == null)
            {
                return;
            }
            try {
                System.out.println("Received content: " + response.content);
                System.out.println("Received status: " + response.statusCode);
                if (response.statusCode == 200) {
                    ObjectMapper mapper = new ObjectMapper();
                    TokenResponse tokenResponse = mapper.readValue(response.content, TokenResponse.class);
                    BotUtils.sendMessage(event.getChannel(), "Connection to Network Manager was successful!");

                    final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
                    config.endpoint = endpoint;
                    config.secret = tokenResponse.token;
                    config.writeYaml();
                } else {
                    ObjectMapper mapper = new ObjectMapper();
                    ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                    BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: " + errorResponse.error);
                }
            } catch(IOException e) {
                e.printStackTrace();
            }
        }
    }
}