package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.Map;

// Only meant for simple commands that have some sub commands...
public abstract class BaseSubCommand extends BaseCommand {

    abstract Map<String, String> getSubCommands();

    String getCompiledHelp(int iteration)
    {
        String help = "\n";
        for (Map.Entry<String, String> subentry : getSubCommands().entrySet()) {
            for (int i = 0; i < iteration; i++) {
                help += "\t";
            }
            help += "\uD83D\uDF84 " + subentry.getKey() + " - " + subentry.getValue() + "\n";
        }
        help = help.substring(0, help.length() - 1);
        return help;
    }

    String getHelp() { return ""; }

    public void run(MessageReceivedEvent event, List<String> args) {
        if (args.size() < 1) {
            String help = "The following are sub-commands of " + getCommand() + "\n";
            help += "```";
            for (Map.Entry<String, String> subentry : getSubCommands().entrySet()) {
                help += subentry.getKey() + " - " + subentry.getValue() + "\n";
            }
            help += "```";
            BotUtils.sendMessage(event.getChannel(), help);
        } else {
            String cmd = args.get(0).toLowerCase();
            if (getSubCommands().containsKey(cmd)) {
                args.remove(0);
                runSub(event, cmd, args);
            } else {
                // log not found command
                BotUtils.sendDebugMessage("Not-Found: BaseSubCommand '" + getCommand() + "' attempted '" + cmd + "'");
                ExtractedResult approximate = FuzzySearch.extractOne(args.get(0), getSubCommands().keySet());
                BotUtils.sendMessage(event.getChannel(), "The sub-command '" + args.get(0) + "' does not exist! Did you mean '" + approximate.getString() + "'?");
            }
        }
    }

    abstract public void runSub(MessageReceivedEvent event, String cmd, List<String> args);
}
