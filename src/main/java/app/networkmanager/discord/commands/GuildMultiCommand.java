package app.networkmanager.discord.commands;

import app.networkmanager.discord.listener.DiscordListener;
import sx.blah.discord.handle.obj.Permissions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class GuildMultiCommand extends BaseMultiCommand {
    public String getCommand() {
        return "guild";
    }

    {
        DiscordListener.addCommand(subCommands, new GuildLinkCommand());
        DiscordListener.addCommand(subCommands, new GuildRankSubCommand());
    }

    public List<Permissions> requiredPermissions() {
        return Arrays.asList(Permissions.ADMINISTRATOR);
    }

    public String getHelp() { return ""; }
}
