package app.networkmanager.discord.commands;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;

import java.util.ArrayList;
import java.util.List;

public abstract class BaseCommand {

    public abstract String getCommand();

    abstract String getHelp();

    String getCompiledHelp(int iteration)
    {
        return getHelp();
    }

    public boolean shouldDeleteMessage() { return false; }

    boolean isSecret() { return false; }

    public List<Permissions> requiredPermissions() { return new ArrayList<>(); }

    public abstract void run(MessageReceivedEvent event, List<String> args);

}
