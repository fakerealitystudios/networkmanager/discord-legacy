package app.networkmanager.discord.commands;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;

public class ChannelClearCommand extends BaseCommand {
    public String getCommand() {
        return "clear";
    }

    public String getHelp() { return "Clear channel of all chat"; }

    public List<Permissions> requiredPermissions() {
        return Arrays.asList(Permissions.MANAGE_MESSAGES);
    }

    public void run(MessageReceivedEvent event, List<String> args) {
        event.getChannel().bulkDelete();
    }
}