package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

import java.util.List;

public class AboutCommand extends BaseCommand {
    public String getCommand() {
        return "about";
    }

    public String getHelp() { return "Talks about NetworkManager"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        EmbedBuilder builder = new EmbedBuilder();
        builder.withAuthorUrl("https://www.networkmanager.io/");
        builder.withAuthorIcon("https://www.networkmanager.io/assets/img/logo.png");
        builder.withAuthorName("NetworkManager");

        String about = "";
        about += "NetworkManager is software being developed in order to provide ease of administration across games, servers, and applications.";
        about += "\n";
        about += "It is being developed by <@164909237011611648> and can be found at https://www.networkmanager.io/.";
        builder.appendField("About", about, false);
        builder.appendField("Discord", "Get support, give feedback, and complain that Max is lazy here: https://discord.networkmanager.io/", false);

        builder.withColor(36,192,235);
        BotUtils.sendMessage(event.getChannel(), builder.build());
    }
}