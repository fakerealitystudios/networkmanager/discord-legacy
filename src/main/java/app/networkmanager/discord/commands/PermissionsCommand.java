package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.listener.DiscordListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class PermissionsCommand extends BaseCommand {
    public String getCommand() {
        return "permissions";
    }

    public String getHelp() { return "Displays required permissions of a command"; }

    public List<Permissions> requiredPermissions() {
        return Arrays.asList(Permissions.MANAGE_ROLES);
    }

    public void run(MessageReceivedEvent event, List<String> args) {
        if (args.size() > 0) {
            if (DiscordListener.commandMap.containsKey(args.get(0))) {
                BaseCommand cmd = DiscordListener.commandMap.get(args.get(0));
                List<Permissions> perms = cmd.requiredPermissions();
                if (perms.size() > 0) {
                    String text = "";
                    //BotUtils.sendMessage();
                    text += "Required Permissions for '" + cmd.getCommand() + "'\n";
                    text += "```\n";
                    for (int i = 0; i < perms.size(); i++) {
                        text += BotUtils.permsMapped.getOrDefault(perms.get(i), "Unknown Permission") + "\n";
                    }
                    text += "```\n";
                    BotUtils.sendMessage(event.getChannel(), text);
                } else {
                    BotUtils.sendMessage(event.getChannel(), "'" + cmd.getCommand() + "' requires no permissions");
                }
            }
        } else {
            String text = "";
            for (Map.Entry<String, BaseCommand> entry : DiscordListener.commandMap.entrySet()) {
                if (entry.getValue().isSecret()) { continue; }
                List<Permissions> perms = entry.getValue().requiredPermissions();
                if (perms.size() > 0) {
                    //BotUtils.sendMessage();
                    text += "Required Permissions for '" + entry.getValue().getCommand() + "'\n";
                    text += "```\n";
                    for (int i = 0; i < perms.size(); i++) {
                        text += BotUtils.permsMapped.getOrDefault(perms.get(i), "Unknown Permission") + "\n";
                    }
                    text += "```\n";
                }
            }
            if (!text.isEmpty())
                BotUtils.sendMessage(event.getChannel(), text);
        }
    }
}