package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.listener.DiscordListener;
import me.xdrop.fuzzywuzzy.FuzzySearch;
import me.xdrop.fuzzywuzzy.model.ExtractedResult;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.Permissions;

import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public abstract class BaseMultiCommand extends BaseCommand {

    Map<String, BaseCommand> subCommands = new TreeMap<>();

    String getCompiledHelp(int iteration)
    {
        String help = "\n";
        for (Map.Entry<String, BaseCommand> subentry : subCommands.entrySet()) {
            if (subentry.getValue().isSecret()) { continue; }
            for (int i = 0; i < iteration; i++) {
                help += "\t";
            }
            help += "\uD83D\uDF84 " + subentry.getValue().getCommand() + " - " + subentry.getValue().getCompiledHelp(iteration + 1) + "\n";
        }
        help = help.substring(0, help.length() - 1);
        return help;
    }

    public void run(MessageReceivedEvent event, List<String> args) {
        if (args.size() < 1) {
            String help = "The following are sub-commands of " + getCommand() + "\n";
            help += "```";
            for (Map.Entry<String, BaseCommand> subentry : subCommands.entrySet()) {
                if (subentry.getValue().isSecret()) { continue; }
                help += subentry.getValue().getCommand() + " - " + subentry.getValue().getCompiledHelp(1) + "\n";
            }
            help += "```";
            BotUtils.sendMessage(event.getChannel(), help);
        } else {
            String cmd = args.get(0).toLowerCase();
            if (subCommands.containsKey(cmd)) {
                args.remove(0);
                DiscordListener.attemptCommand(event, args, subCommands.get(cmd));
            } else {
                // log command not found
                BotUtils.sendDebugMessage("Not-Found: BaseMultiCommand '" + getCommand() + "' attempted '" + cmd + "'");

                ExtractedResult approximate = FuzzySearch.extractOne(args.get(0), subCommands.keySet());
                BotUtils.sendMessage(event.getChannel(), "The sub-command '" + args.get(0) + "' does not exist! Did you mean '" + approximate.getString() + "'?");
            }
        }
    }
}
