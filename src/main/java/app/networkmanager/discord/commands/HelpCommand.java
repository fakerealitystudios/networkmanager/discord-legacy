package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.listener.DiscordListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

import java.util.List;
import java.util.Map;

public class HelpCommand extends BaseCommand {
    public String getCommand() {
        return "help";
    }

    String getHelp() { return "Used to display a list of available commands"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        if (DiscordListener.commandMap.size() <= 1) {
            BotUtils.sendMessage(event.getChannel(), "No commands have been registered!");
        } else {
            String help = "The following are the commands I have at my disposal:\n";
            help += "```";
            for (Map.Entry<String, BaseCommand> entry : DiscordListener.commandMap.entrySet()) {
                BaseCommand baseCommand = entry.getValue();
                if (baseCommand.isSecret()) { continue; }
                help += "⚬ " + baseCommand.getCommand() + " - " + baseCommand.getCompiledHelp(1) + "\n";
            }
            help += "```";
            BotUtils.sendMessage(event.getChannel(), help);
        }
    }
}
