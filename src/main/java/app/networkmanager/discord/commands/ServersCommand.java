package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.ErrorResponse;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.api.responses.ServerResponse;
import app.networkmanager.discord.api.responses.ServersResponse;
import app.networkmanager.discord.configuration.GuildConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

import java.io.IOException;
import java.util.List;

public class ServersCommand extends BaseCommand {
    public String getCommand() {
        return "servers";
    }

    public String getHelp() { return "Lists all servers connected with network manager"; }

    public void run(MessageReceivedEvent event, List<String> args) {
        final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());

        if (config.secret.isEmpty()) {
            BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: Your panel hasn't been linked yet!");
            return;
        }

        event.getChannel().setTypingStatus(true);
        GenericResponse response = Main.Api.GET.makeRequest(config.endpoint + "api.php?realm=discord&action=servers", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
        if (response == null)
        {
            return;
        }
        try {
            System.out.println("Received content: " + response.content);
            System.out.println("Received status: " + response.statusCode);
            if (response.statusCode == 200) {
                ObjectMapper mapper = new ObjectMapper();
                ServersResponse mappedResponse = mapper.readValue(response.content, ServersResponse.class);
                //BotUtils.sendMessage(event.getChannel(), "Received server list!");
                /*String text = "Current list of registered servers:";
                text += "\n```";
                for (ServerResponse server : mappedResponse.servers) {
                    text += server.toString() + "\n";
                }
                text += "```\n";
                BotUtils.sendMessage(event.getChannel(), text);*/

                EmbedBuilder builder = new EmbedBuilder();
                for (ServerResponse server : mappedResponse.servers) {
                    builder.appendField(server.name, server.getAddress(), false);
                }
                builder.withColor(36,192,235);
                builder.withTitle("Current Servers");
                BotUtils.sendMessage(event.getChannel(), builder.build());
            } else {
                ObjectMapper mapper = new ObjectMapper();
                ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: " + errorResponse.error);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
