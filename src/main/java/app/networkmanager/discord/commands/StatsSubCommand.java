package app.networkmanager.discord.commands;

import app.networkmanager.discord.BotUtils;
import app.networkmanager.discord.Main;
import app.networkmanager.discord.api.responses.*;
import app.networkmanager.discord.configuration.GuildConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.EmbedBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class StatsSubCommand extends BaseSubCommand {
    public String getCommand() {
        return "stats";
    }

    public Map<String, String> getSubCommands()
    {
        Map<String, String> map = new TreeMap<>();
        map.put("me", "Displays your own statistics");
        map.put("playtime", "List the top 10 players by playtime");
        map.put("sessions", "List the top 10 players by sessions");
        map.put("kills", "List the top 10 players by kills");
        map.put("deaths", "List the top 10 players by deaths");
        return map;
    }

    public void runSub(MessageReceivedEvent event, String cmd, List<String> args) {
        final GuildConfiguration config = Main.GetGuildConfiguration(event.getGuild().getStringID());
        if (config.secret.isEmpty()) {
            BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: Your panel hasn't been linked yet!");
            return;
        }
        GenericResponse response = null;
        event.getChannel().setTypingStatus(true);
        String title = "";

        switch (cmd) {
            case "me":
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("memberLink", "discord"));
                params.add(new BasicNameValuePair("memberToken", event.getAuthor().getStringID()));
                try {
                    response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=statistics&stat=me", event.getChannel(), new UrlEncodedFormEntity(params), new BasicNameValuePair("Authorization", config.secret));
                    System.out.println("Received content: " + response.content);
                    System.out.println("Received status: " + response.statusCode);
                    if (response.statusCode == 200) {
                        ObjectMapper mapper = new ObjectMapper();
                        StatisticsMeResponse mappedResponse = mapper.readValue(response.content, StatisticsMeResponse.class);

                        EmbedBuilder builder = new EmbedBuilder();
                        builder.withAuthorUrl(mappedResponse.url);
                        builder.withAuthorIcon(mappedResponse.avatar);
                        builder.withAuthorName(mappedResponse.displayName);
                        if (mappedResponse.playtime == null)
                            mappedResponse.playtime = "None";
                        builder.appendField("Playtime", mappedResponse.playtime, false);
                        if (mappedResponse.sessions == null)
                            mappedResponse.sessions = "0";
                        builder.appendField("Sessions", mappedResponse.sessions, false);
                        if (mappedResponse.kills == null)
                            mappedResponse.kills = "0";
                        builder.appendField("Kills", mappedResponse.kills, false);
                        if (mappedResponse.deaths == null)
                            mappedResponse.deaths = "0";
                        builder.appendField("Deaths", mappedResponse.deaths, false);
                        builder.withColor(36,192,235);
                        BotUtils.sendMessage(event.getChannel(), builder.build());
                    } else {
                        ObjectMapper mapper = new ObjectMapper();
                        ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                        BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: " + errorResponse.error);
                    }
                } catch(Exception e) {
                    e.printStackTrace();
                }
                response = null;
                break;
            case "playtime":
                title = "Playtime";
                response = Main.Api.GET.makeRequest(config.endpoint + "api.php?realm=discord&action=statistics&stat=playtime", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
                break;
            case "sessions":
                title = "Sessions";
                response = Main.Api.GET.makeRequest(config.endpoint + "api.php?realm=discord&action=statistics&stat=sessions", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
                break;
            case "kills":
                title = "Kills";
                response = Main.Api.GET.makeRequest(config.endpoint + "api.php?realm=discord&action=statistics&stat=kills", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
                break;
            case "deaths":
                title = "Deaths";
                response = Main.Api.GET.makeRequest(config.endpoint + "api.php?realm=discord&action=statistics&stat=deaths", event.getChannel(), new BasicNameValuePair("Authorization", config.secret));
                break;
        }

        if (response == null)
        {
            return;
        }
        try {
            System.out.println("Received content: " + response.content);
            System.out.println("Received status: " + response.statusCode);
            if (response.statusCode == 200) {
                ObjectMapper mapper = new ObjectMapper();
                StatisticsResponse mappedResponse = mapper.readValue(response.content, StatisticsResponse.class);

                EmbedBuilder builder = new EmbedBuilder();
                String top = "";
                int i = 0;
                for (StatisticsValue stat : mappedResponse.top) {
                    if (i > 0)
                        top += "\n";
                    i++;
                    top += "[" + stat.name + "](" + stat.url + ")";
                }
                builder.appendField("Players", top, true);
                top = "";
                i = 0;
                for (StatisticsValue stat : mappedResponse.top) {
                    if (i > 0)
                        top += "\n";
                    i++;
                    top += stat.value;
                }
                builder.appendField(title, top, true);
                builder.withTitle("Top Players by " + title);
                builder.withColor(36,192,235);
                BotUtils.sendMessage(event.getChannel(), builder.build());
            } else {
                ObjectMapper mapper = new ObjectMapper();
                ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                BotUtils.sendMessage(event.getChannel(), "Failed to connect to Network Manager: " + errorResponse.error);
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}

