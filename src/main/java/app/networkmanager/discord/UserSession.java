package app.networkmanager.discord;

import app.networkmanager.discord.api.responses.ErrorResponse;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.api.responses.MemberResponse;
import app.networkmanager.discord.api.responses.UserSessionResponse;
import app.networkmanager.discord.configuration.GuildConfiguration;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.util.EmbedBuilder;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class UserSession {
    public int nmid;
    public int sessionid;
    private GuildConfiguration config;

    public UserSession(IGuild guild, int nmid) {
        this.nmid = nmid;
        config = Main.GetGuildConfiguration(guild.getStringID());

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("nmid", Integer.toString(nmid)));
        UrlEncodedFormEntity ent = null;
        try {
            ent = new UrlEncodedFormEntity(params);
        } catch(UnsupportedEncodingException e) {
            return;
        }

        GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=usersession", null, ent, new BasicNameValuePair("Authorization", config.secret));
        System.out.println("Received content: " + response.content);
        System.out.println("Received status: " + response.statusCode);
        try {
            if (response.statusCode == 200) {
                ObjectMapper mapper = new ObjectMapper();
                UserSessionResponse mappedResponse = mapper.readValue(response.content, UserSessionResponse.class);

                sessionid = Integer.parseInt(mappedResponse.usid);
            } else {
                ObjectMapper mapper = new ObjectMapper();
                ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                BotUtils.sendDebugMessage("Failed to connect to Network Manager: " + errorResponse.error);
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void close() {
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("nmid", Integer.toString(nmid)));
        params.add(new BasicNameValuePair("usid", Integer.toString(sessionid)));
        UrlEncodedFormEntity ent = null;
        try {
            ent = new UrlEncodedFormEntity(params);
        } catch(UnsupportedEncodingException e) {
            return;
        }

        GenericResponse response = Main.Api.POST.makeRequest(config.endpoint + "api.php?realm=discord&action=usersession", null, ent, new BasicNameValuePair("Authorization", config.secret));
        System.out.println("Received content: " + response.content);
        System.out.println("Received status: " + response.statusCode);

        sessionid = -1;
    }
}
