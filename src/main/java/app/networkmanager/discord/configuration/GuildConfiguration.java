package app.networkmanager.discord.configuration;

import java.util.HashMap;
import java.util.Map;
import java.io.File;

public class GuildConfiguration extends BaseConfiguration {

    @Override
    public int getConfigVersion() {
        return 4;
    }
    
    public String name = "";
    public String endpoint = "https://demo.networkmanager.io/";
    public String secret = ""; // defaults to blank. Secrets are created inside of the nm web panel
    public Map<String, ChannelConfiguration> channels = new HashMap<>();
    // Badge -> Discord Role
    public Map<String, String> badgesMap = new HashMap<>();
    // NetworkManager Rank -> Discord Role
    public Map<String, String> ranksMap = new HashMap<>();
    // Discord Role -> NetworkManager Rank
    public Map<String, String> rolesMap = new HashMap<>();

    public static GuildConfiguration initialize(String guild) {
        File folder = new File("guilds");
        if (!folder.exists())
            folder.mkdir();
        return BaseConfiguration.readYaml(new File("guilds/" + guild + ".yml"), GuildConfiguration.class);
    }

    public ChannelConfiguration getChannel(String channel) {
        if (!channels.containsKey(channel)) {
            channels.put(channel, new ChannelConfiguration());
        }
        return channels.get(channel);
    }
}
