package app.networkmanager.discord.configuration;

import java.io.File;

public class MainConfiguration extends BaseConfiguration {

    @Override
    public int getConfigVersion() {
        return 1;
    }

    public String botToken = "";
	public String apiAddress = "https://api.networkmanager.app";

    public static MainConfiguration initialize() {
        return BaseConfiguration.readYaml(new File("config.yml"), MainConfiguration.class);
    }
}
