package app.networkmanager.discord.configuration;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.File;
import java.io.FileOutputStream;

public class BaseConfiguration {
    private ObjectMapper mapper;
    private File file;

    // BaseConfiguration Values(ALWAYS set the initializer!)
    public int version = 1;

    @JsonIgnore
    public int getConfigVersion() {
        return 1;
    }

    public static <T extends BaseConfiguration> T readYaml(final File file, Class<T> configClass) {
        if (!file.exists()) {
            createYaml(file, configClass);
        }
        try {
            final ObjectMapper mapper = new ObjectMapper(new YAMLFactory()); // jackson databind
            BaseConfiguration config = mapper.readValue(file, configClass);
            config.mapper = mapper;
            config.file = file;
            if (config.version != config.getConfigVersion()) {
                System.out.println("Config version mismatch! Performing update.");
                config.version = config.getConfigVersion();
                config.writeYaml();
            }
            return (T)config;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void writeYaml() {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            SequenceWriter sw = mapper.writerWithDefaultPrettyPrinter().writeValues(file);
            sw.write(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void createYaml(final File file, Class<? extends BaseConfiguration> configClass) {
        try {
            final ObjectMapper mapper = new ObjectMapper(new YAMLFactory()); // jackson databind
            FileOutputStream fos = new FileOutputStream(file);
            SequenceWriter sw = mapper.writerWithDefaultPrettyPrinter().writeValues(file);
            sw.write(configClass.getConstructor().newInstance());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
