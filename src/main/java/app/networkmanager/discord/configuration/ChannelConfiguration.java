package app.networkmanager.discord.configuration;

public class ChannelConfiguration {
    // Disregard commands in this channel?
    public boolean ignored = false;
    // Cooldown between user messages
    public int cooldown = -1;
}
