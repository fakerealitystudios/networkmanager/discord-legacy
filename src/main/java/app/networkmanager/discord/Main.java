package app.networkmanager.discord;

import app.networkmanager.discord.api.ApiRequests;
import app.networkmanager.discord.api.responses.ErrorResponse;
import app.networkmanager.discord.api.responses.GenericResponse;
import app.networkmanager.discord.api.responses.MemberResponse;
import app.networkmanager.discord.configuration.GuildConfiguration;
import app.networkmanager.discord.configuration.MainConfiguration;
import app.networkmanager.discord.listener.DiscordListener;
import app.networkmanager.discord.listener.DiscordLogListener;
import app.networkmanager.discord.listener.ExceptionListener;
import app.networkmanager.discord.listener.SocketListener;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.handle.obj.StatusType;
import sx.blah.discord.util.RequestBuffer;

import java.sql.*;

import java.util.*;

public class Main {
    public static IDiscordClient DiscordClient;
    public static DiscordListener DiscordListener;
    public static MainConfiguration Config;
    public static Connection SQLite;
    public static ApiRequests Api;
    public static SocketListener socket;
    public static Map<String, GuildConfiguration> GuildConfigurations = new HashMap<>();
    public static Map<String, Map<String, UserSession>> UserSessions = new HashMap<>();

    public static void main(String[] args){
        Config = MainConfiguration.initialize();

        if (Config.botToken.isEmpty()) {
            System.out.println("Open the config.yml and set your bot token!");
            return;
        }

        try {
            SQLite = DriverManager.getConnection("jdbc:sqlite:sqlite.db");
        } catch(SQLException e) {
            e.printStackTrace();
        }
        if (SQLite == null) {
            System.out.println("Failed to open/create sqlite.db file!");
            return;
        } else {
            try {
                String sql;
                PreparedStatement stmt;
                // discordid is guildid.clientid
				sql = "CREATE TABLE IF NOT EXISTS `members` (`discordid` TEXT NOT NULL UNIQUE, `nmid` INTEGER NOT NULL);";
                stmt = SQLite.prepareStatement(sql);
                stmt.execute();
                stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        Thread.setDefaultUncaughtExceptionHandler(new ExceptionListener());
        Api = new ApiRequests();
        DiscordClient = BotUtils.getBuiltDiscordClient(Config.botToken);

        // Register a listener via the EventSubscriber annotation which allows for organisation and delegation of events
        DiscordListener = new DiscordListener();
        DiscordClient.getDispatcher().registerListener(DiscordListener);
        DiscordClient.getDispatcher().registerListener(new DiscordLogListener());

        // Only login after all events are registered otherwise some may be missed.
        DiscordClient.login();

        socket = new SocketListener(21055);
        socket.start();

        Scanner sn = new Scanner(System.in);
        while (true) {
            try {
                String[] argsArray = sn.nextLine().split(" ");
                String cmd = argsArray[0];
                List<String> argsList = new ArrayList<>(Arrays.asList(argsArray));
                argsList.remove(0);
                if (cmd.equalsIgnoreCase("quit")) {
                    socket.serverSocket.close();
                    DiscordClient.changePresence(StatusType.OFFLINE);
                    DiscordClient.logout();
                    break;
                } else if (cmd.equalsIgnoreCase("status")) {
                    if (DiscordClient.isLoggedIn()) {
                        System.out.println("Discord bot is logged in.");
                    }
                } else if (cmd.equalsIgnoreCase("debug")) {
                    BotUtils.sendDebugMessage(String.join(" ", argsList));
                } else if (cmd.equalsIgnoreCase("msg")) {
                    argsList.remove(0);
                    BotUtils.sendMessage(DiscordClient.getChannelByID(Long.parseLong(argsArray[1])), "[" + String.join(" ", argsList) + "]");
                }
            } catch (Exception e) { // Take all the errors from this. This should not interfere with the main discord functions
                e.printStackTrace();
            }
        }

    }

    public static void LoadGuildConfiguration(String guild) {
        GuildConfigurations.put(guild, GuildConfiguration.initialize(guild));
    }

    public static GuildConfiguration GetGuildConfiguration(String guild) {
        return GuildConfigurations.get(guild);
    }

    public static int GetUserNetworkManagerId(IGuild guild, IUser user) {
        try {
            PreparedStatement stmt = Main.SQLite
                    .prepareStatement("SELECT `nmid` FROM `members` WHERE `discordid` = ?");
            stmt.setString(1, guild.getStringID() + "." + user.getStringID());
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int nmid = rs.getInt("nmid");
                rs.close();
                stmt.close();
                return nmid;
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public static IUser GetUserDiscordUser(IGuild guild, int nmid) {
        try {
            PreparedStatement stmt = Main.SQLite
                    .prepareStatement("SELECT `discordid` FROM `members` WHERE `discordid` LIKE ? AND `nmid` = ?");
            stmt.setString(1, guild.getStringID() + ".%");
            stmt.setInt(2, nmid);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                String discordid = rs.getString("discordid").substring(guild.getStringID().length() + 1, rs.getString("discordid").length());
                rs.close();
                stmt.close();
                return Main.DiscordClient.getUserByID(Long.parseLong(discordid));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void CollectUser(IGuild guild, IUser user) {
        final GuildConfiguration config = Main.GetGuildConfiguration(guild.getStringID());

        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("memberLink", "discord"));
        params.add(new BasicNameValuePair("memberToken", user.getStringID()));
        params.add(new BasicNameValuePair("displayName", user.getDisplayName(guild)));
        try {
            GenericResponse response = Main.Api.POST.makeRequest(
                    config.endpoint + "api.php?realm=discord&action=user", null,
                    new UrlEncodedFormEntity(params), new BasicNameValuePair("Authorization", config.secret));
            System.out.println("Received content: " + response.content);
            System.out.println("Received status: " + response.statusCode);
            if (response.statusCode == 200) {
                ObjectMapper mapper = new ObjectMapper();
                MemberResponse mappedResponse = mapper.readValue(response.content, MemberResponse.class);

                PreparedStatement stmt = Main.SQLite
                        .prepareStatement("INSERT OR REPLACE INTO `members`(`discordid`,`nmid`) VALUES (?, ?)");
                stmt.setString(1, guild.getStringID() + "." + user.getStringID());
                stmt.setInt(2, Integer.parseInt(mappedResponse.id));
                stmt.execute();
                stmt.close();

                // Go through current roles and remove any that the user doesn't have
                List<IRole> roles = user.getRolesForGuild(guild);
                for (IRole role : roles) {
                    if (config.rolesMap.containsKey(role.getStringID())) {
                        String mrank = config.rolesMap.get(role.getStringID());
                        boolean bFound = false;
                        for (MemberResponse.Rank rank : mappedResponse.ranks) {
                            if (rank.rank.equalsIgnoreCase(mrank)) {
                                bFound = true;
                                break;
                            }
                        }
                        if (!bFound) {
                            RequestBuffer.request(() -> {
                                user.removeRole(role);
                            });
                        }
                    }
                }

                // Now perform setting ranks
                for (MemberResponse.Rank rank : mappedResponse.ranks) {
                    if (config.ranksMap.containsKey(rank.rank)) {
                        IRole role = guild.getRoleByID(Long.parseLong(config.ranksMap.get(rank.rank)));
                        if (!roles.contains(role)) {
                            // Give user missing role
                            RequestBuffer.request(() -> {
                                user.addRole(role);
                            });
                        }
                    }
                }
                for (String badge : mappedResponse.badges) {
                    if (config.badgesMap.containsKey(badge)) {
                        IRole role = guild.getRoleByID(Long.parseLong(config.badgesMap.get(badge)));
                        if (!roles.contains(role)) {
                            // Give user missing role
                            RequestBuffer.request(() -> {
                                user.addRole(role);
                            });
                        }
                    }
                }

                System.out.println("Loaded discord user " + user.getName() + " NMID " + mappedResponse.id);
            } else {
                ObjectMapper mapper = new ObjectMapper();
                ErrorResponse errorResponse = mapper.readValue(response.content, ErrorResponse.class);
                BotUtils.sendDebugMessage("Failed to connect to Network Manager: " + errorResponse.error);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
